import boto3
from IPython.display import Image, SVG, display

s3 = boto3.resource('s3')
bucket_name = 'archnn-upload'
svg_bucket_name = 'archnn-svg'

def upload_image(filename):
    s3.Object(bucket_name, filename).put(Body=open(filename))

def upload_svg(filename):
    s3.Object(svg_bucket_name, filename).put(Body=open(filename))

def image_url(filename):
    return "https://s3-eu-west-1.amazonaws.com/archnn-upload/{}".format(filename)

def svg_url(filename):
    return "https://s3-eu-west-1.amazonaws.com/archnn-upload/{}".format(filename)
