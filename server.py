from uuid import uuid4
from flask import (Flask, current_app, request, redirect,
    render_template)
app = Flask(__name__)

@app.route('/')
def index():
    return current_app.send_static_file('index.html')

@app.route('/upload', methods=['POST'])
def upload():
    f = request.files['file']
    uid = uuid4()
    return redirect("/diagram/{}".format(uid))

@app.route('/diagram/<uid>')
def diagram(uid):
    filename = 'example1.svg'
    try:
        with open(filename, 'r') as f:
            svg_diagram=f.read().replace('\n','')
            return render_template('show_svg.html', svg_diagram=svg_diagram)
    except IOError as e:
        print e
        return current_app.send_static_file('500.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0')

